;(self['webpackChunkvue3_test'] = self['webpackChunkvue3_test'] || []).push([
  [143],
  {
    6058: function (e, t, n) {
      var r = { './demo.ts': 9284 }
      function o(e) {
        var t = a(e)
        return n(t)
      }
      function a(e) {
        if (!n.o(r, e)) {
          var t = new Error("Cannot find module '" + e + "'")
          throw ((t.code = 'MODULE_NOT_FOUND'), t)
        }
        return r[e]
      }
      ;(o.keys = function () {
        return Object.keys(r)
      }),
        (o.resolve = a),
        (e.exports = o),
        (o.id = 6058)
    },
    9284: function (e, t, n) {
      'use strict'
      n.r(t),
        (t['default'] = [
          {
            url: '/auth/oauth/token',
            method: 'get',
            response() {
              return { code: 200, msg: '成功', data: { name: 'test get token' } }
            },
          },
        ])
    },
    9816: function (e, t, n) {
      'use strict'
      var r = n(9242)
      n(6699)
      function o() {
        return 'tokenxxxxx'
      }
      function a(e) {
        return [e].includes('production')
      }
      var u = n(3396)
      function s(e, t, n, r, o, a) {
        const s = (0, u.up)('router-view')
        return (0, u.wg)(), (0, u.j4)(s)
      }
      var c = (0, u.aZ)({ name: 'App', setup() {} }),
        i = n(89)
      const d = (0, i.Z)(c, [['render', s]])
      var l = d,
        f = n(678),
        p = (n(7998), n(3135), n(1263)),
        v = n(8079)
      function m(e, t, n, r, o, a) {
        const s = (0, u.up)('v-aside'),
          c = (0, u.up)('v-header'),
          i = (0, u.up)('v-main'),
          d = (0, u.up)('v-footer'),
          l = v.G4,
          f = p.BR
        return (
          (0, u.wg)(),
          (0, u.j4)(
            f,
            { size: e.size, 'z-index': e.zIndex, locale: e.locale },
            {
              default: (0, u.w5)(() => [
                (0, u.Wm)(
                  l,
                  { class: 'layout' },
                  {
                    default: (0, u.w5)(() => [
                      (0, u.Wm)(s),
                      (0, u.Wm)(
                        l,
                        { class: 'layout-container' },
                        { default: (0, u.w5)(() => [(0, u.Wm)(c), (0, u.Wm)(i), (0, u.Wm)(d)]), _: 1 }
                      ),
                    ]),
                    _: 1,
                  }
                ),
              ]),
              _: 1,
            },
            8,
            ['size', 'z-index', 'locale']
          )
        )
      }
      var h = n(50)
      n(116)
      const w = (0, u.Uk)('Aside')
      function k(e, t, n, r, o, a) {
        const s = v.$w
        return (0, u.wg)(), (0, u.j4)(s, { width: '200px' }, { default: (0, u.w5)(() => [w]), _: 1 })
      }
      var _ = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const g = (0, i.Z)(_, [
        ['render', k],
        ['__scopeId', 'data-v-7ad4a520'],
      ])
      var Z = g
      n(7147)
      const x = (0, u.Uk)('Header')
      function y(e, t, n, r, o, a) {
        const s = v.nZ
        return (0, u.wg)(), (0, u.j4)(s, null, { default: (0, u.w5)(() => [x]), _: 1 })
      }
      var E = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const O = (0, i.Z)(E, [
        ['render', y],
        ['__scopeId', 'data-v-eaf284e2'],
      ])
      var D = O
      n(6909)
      const b = (0, u.Uk)('Main ')
      function j(e, t, n, r, o, a) {
        const s = (0, u.up)('router-view'),
          c = v.b2
        return (0, u.wg)(), (0, u.j4)(c, null, { default: (0, u.w5)(() => [b, (0, u.Wm)(s)]), _: 1 })
      }
      var U = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const z = (0, i.Z)(U, [
        ['render', j],
        ['__scopeId', 'data-v-4df92d9c'],
      ])
      var L = z
      n(2122)
      const A = (0, u.Uk)('Footer')
      function I(e, t, n, r, o, a) {
        const s = v.F_
        return (0, u.wg)(), (0, u.j4)(s, null, { default: (0, u.w5)(() => [A]), _: 1 })
      }
      var M = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const W = (0, i.Z)(M, [
        ['render', I],
        ['__scopeId', 'data-v-0af72e52'],
      ])
      var C = W,
        F = (0, u.aZ)({
          components: { ElConfigProvider: p.BR, VAside: Z, VHeader: D, VMain: L, VFooter: C },
          setup() {
            return { locale: h.Z, zIndex: 3e3, size: 'small' }
          },
        })
      const N = (0, i.Z)(F, [
        ['render', m],
        ['__scopeId', 'data-v-4880349a'],
      ])
      var H = N
      function T(e, t, n, r, o, a) {
        return (0, u.wg)(), (0, u.iD)('div', null, '登录')
      }
      var V = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const R = (0, i.Z)(V, [['render', T]])
      var B = R,
        $ = n(7139),
        G = n(4870),
        K = (0, u.aZ)({
          name: 'Home',
          setup(e) {
            const t = (0, G.iH)([
              { id: '1', text: 'test1' },
              { id: '2', text: 'test2' },
            ])
            return (e, n) => (
              (0, u.wg)(),
              (0, u.iD)('div', null, [
                ((0, u.wg)(!0),
                (0, u.iD)(
                  u.HY,
                  null,
                  (0, u.Ko)(t.value, e => ((0, u.wg)(), (0, u.iD)('div', { key: e.id }, (0, $.zw)(e.text), 1))),
                  128
                )),
              ])
            )
          },
        })
      const P = K
      var Y = P
      function q(e, t, n, r, o, a) {
        return (0, u.wg)(), (0, u.iD)('div', null, 'Error')
      }
      var J = (0, u.aZ)({
        setup() {
          return {}
        },
      })
      const Q = (0, i.Z)(J, [['render', q]])
      var S = Q
      const X = [],
        ee = n(6762)
      ee.keys().forEach(e => {
        X.push(...(ee(e).default || []))
      })
      const te = [
          { path: '/login', name: 'Login', component: B },
          {
            path: '/',
            name: 'Layout',
            component: H,
            children: [
              { path: '/:pathMatch(.*)*', redirect: '/error' },
              { path: '/', redirect: '/home' },
              { path: '/home', name: 'home', component: Y, meta: { keepAlive: !1, title: '首页' } },
              { path: '/error', name: 'Error', component: S, meta: { keepAlive: !1, title: '404' } },
              ...X,
            ],
          },
        ],
        ne = (0, f.p7)({ history: (0, f.r5)(), routes: te })
      var re = ne
      const oe = [],
        ae = n(6762)
      ae.keys().forEach(e => {
        oe.push(...(ae(e).default || []))
      })
      const ue = oe.map(e => e.name)
      var se = ue
      const ce = a('debug'),
        ie = ['Login', 'Error']
      ce && ie.push(...se),
        re.beforeEach((e, t, n) => {
          const r = e.name
          if (ie.includes(r)) return void n()
          const a = e.matched.length && 'Layout' === e.matched[0].name,
            u = o()
          a && u ? n() : n('/login')
        })
      var de = re,
        le = n(65)
      const fe = {},
        pe = n(5295)
      pe.keys().forEach(e => {
        const t = e.match(/^\.\/(.*)\.ts|.js$/),
          n = t && t[1]
        n && (fe[n] = pe(e).default)
      })
      const ve = {},
        me = {},
        he = {},
        we = {}
      var ke = (0, le.MT)({ modules: fe, state: ve, getters: me, mutations: he, actions: we }),
        _e = n(7634),
        ge = n.n(_e)
      const Ze = [],
        xe = n(6058)
      xe.keys().forEach(e => {
        Ze.push(...(xe(e).default || []))
      })
      const ye = [...Ze]
      function Ee() {
        for (const e of ye) ge().mock(new RegExp(e.url), e.method || 'get', e.response)
      }
      ge().setup({ timeout: '300-1000' }), a('mock') && Ee()
      const Oe = (0, r.ri)(l)
      Oe.use(ke).use(de).mount('#app')
    },
    3783: function (e, t, n) {
      'use strict'
      n.r(t)
      const r = '/order',
        o = [
          {
            path: r + '/orderList',
            name: 'OrderList',
            component: () => n.e(844).then(n.bind(n, 1262)),
            meta: { leaf: !1, keepAlive: !1, title: '订单列表' },
          },
          {
            path: r + '/orderDetail/:id',
            name: 'OrderDetail',
            component: () => n.e(844).then(n.bind(n, 2518)),
            meta: { leaf: !1, keepAlive: !1, title: '订单详情' },
          },
        ]
      t['default'] = o
    },
    9335: function (e, t, n) {
      'use strict'
      n.r(t)
      const r = {},
        o = {},
        a = {},
        u = {}
      t['default'] = { namespaced: !0, state: r, getters: o, mutations: a, actions: u }
    },
    6762: function (e, t, n) {
      var r = { './order.ts': 3783 }
      function o(e) {
        var t = a(e)
        return n(t)
      }
      function a(e) {
        if (!n.o(r, e)) {
          var t = new Error("Cannot find module '" + e + "'")
          throw ((t.code = 'MODULE_NOT_FOUND'), t)
        }
        return r[e]
      }
      ;(o.keys = function () {
        return Object.keys(r)
      }),
        (o.resolve = a),
        (e.exports = o),
        (o.id = 6762)
    },
    5295: function (e, t, n) {
      var r = { './demo.ts': 9335 }
      function o(e) {
        var t = a(e)
        return n(t)
      }
      function a(e) {
        if (!n.o(r, e)) {
          var t = new Error("Cannot find module '" + e + "'")
          throw ((t.code = 'MODULE_NOT_FOUND'), t)
        }
        return r[e]
      }
      ;(o.keys = function () {
        return Object.keys(r)
      }),
        (o.resolve = a),
        (e.exports = o),
        (o.id = 5295)
    },
  },
  function (e) {
    var t = function (t) {
      return e((e.s = t))
    }
    e.O(0, [998], function () {
      return t(9816)
    })
    e.O()
  },
])
